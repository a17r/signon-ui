import QtQuick 2.0

Loader {
    id: loader
    anchors {
        fill: parent
        bottomMargin: osk.height
    }
    focus: true
    sourceComponent: browserComponent
}
