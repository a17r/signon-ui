import QtQuick 2.0

Item {
    id: root
    width: 600
    height: 400
    property var signonRequest: request

    Loader {
        id: loader

        property Component webView: browserComponent
        property Item osk: osk

        anchors.fill: parent
        focus: true
        source: request.pageComponentUrl
    }

    KeyboardRectangle {
        id: osk
    }

    Component {
        id: browserComponent
        WebView {
        }
    }
}
