import QtQuick 2.0

Item {
    id: root

    property real value: 0

    Rectangle {
        anchors { left: parent.left; top: parent.top; bottom: parent.bottom }
        color: "red"
        width: root.value * root.width
    }
}
