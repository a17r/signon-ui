import QtQuick 2.0
import QtWebEngine 1.1

WebEngineView {
    id: root

    property bool lastLoadFailed: false

    Component.onCompleted: url = signonRequest.startUrl

    onLoadingChanged: loadingInfo => {
        console.log("Loading changed")
        if (loading && !lastLoadFailed) {
            signonRequest.onLoadStarted()
        } else if (loadingInfo.status == WebEngineView.LoadSucceededStatus) {
            lastLoadFailed = false
            signonRequest.onLoadFinished(true)
        } else if (loadingInfo.status == WebEngineView.LoadFailedStatus) {
            lastLoadFailed = true
            signonRequest.onLoadFinished(false)
        }
    }
    onUrlChanged: signonRequest.currentUrl = url

    profile: WebEngineProfile {
        cachePath: rootDir
        persistentStoragePath: rootDir
    }

    ProgressBar {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: 6
        visible: root.loading
        value: root.loadProgress / 100
    }
}
