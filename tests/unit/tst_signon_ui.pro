include(../../common-project-config.pri)
include($${TOP_SRC_DIR}/common-vars.pri)

TARGET = signon-ui-unittest

CONFIG += \
    build_all \
    debug \
    link_pkgconfig \
    qtestlib

QT += \
    core \
    dbus \
    gui \
    network \
    quick

lessThan(QT_MAJOR_VERSION, 6) {
    QT += webengine
} else {
    QT += webenginequick
}

PKGCONFIG += \
    signon-plugins-common \
    libnotify

QT += \
    widgets
PKGCONFIG += \
    accounts-qt$$QT_MAJOR_VERSION \
    libsignon-qt$$QT_MAJOR_VERSION

SOURCES += \
    fake-libnotify.cpp \
    fake-libsignon.cpp \
    fake-webcredentials-interface.cpp \
    test.cpp \
    $$TOP_SRC_DIR/src/animation-label.cpp \
    $$TOP_SRC_DIR/src/browser-request.cpp \
    $$TOP_SRC_DIR/src/cookie-jar-manager.cpp \
    $$TOP_SRC_DIR/src/debug.cpp \
    $$TOP_SRC_DIR/src/dialog-request.cpp \
    $$TOP_SRC_DIR/src/dialog.cpp \
    $$TOP_SRC_DIR/src/http-warning.cpp \
    $$TOP_SRC_DIR/src/i18n.cpp \
    $$TOP_SRC_DIR/src/indicator-service.cpp \
    $$TOP_SRC_DIR/src/network-access-manager.cpp \
    $$TOP_SRC_DIR/src/qquick-dialog.cpp \
    $$TOP_SRC_DIR/src/reauthenticator.cpp \
    $$TOP_SRC_DIR/src/request.cpp \
    $$OUT_PWD/../../src/webcredentials_adaptor.cpp
HEADERS += \
    fake-libnotify.h \
    fake-webcredentials-interface.h \
    test.h \
    $$TOP_SRC_DIR/src/animation-label.h \
    $$TOP_SRC_DIR/src/browser-request.h \
    $$TOP_SRC_DIR/src/debug.h \
    $$TOP_SRC_DIR/src/cookie-jar-manager.h \
    $$TOP_SRC_DIR/src/dialog-request.h \
    $$TOP_SRC_DIR/src/dialog.h \
    $$TOP_SRC_DIR/src/http-warning.h \
    $$TOP_SRC_DIR/src/indicator-service.h \
    $$TOP_SRC_DIR/src/network-access-manager.h \
    $$TOP_SRC_DIR/src/qquick-dialog.h \
    $$TOP_SRC_DIR/src/reauthenticator.h \
    $$TOP_SRC_DIR/src/request.h \
    $$OUT_PWD/../../src/webcredentials_adaptor.h

INCLUDEPATH += \
    . \
    $$TOP_SRC_DIR/src \
    $$OUT_PWD/../../src

QMAKE_CXXFLAGS += \
    -fno-exceptions \
    -fno-rtti

DEFINES += \
    DEBUG_ENABLED \
    UNIT_TESTS

RESOURCES += $$TOP_SRC_DIR/src/animationlabel.qrc

check.depends = $${TARGET}
check.commands = "xvfb-run -a dbus-test-runner -t ./signon-ui-unittest"
QMAKE_EXTRA_TARGETS += check
